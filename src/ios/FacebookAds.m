/********* FacebookAds.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <UIKit/UIKit.h>
#import <FBAudienceNetwork/FBAudienceNetwork.h>
#import <AdSupport/ASIdentifierManager.h>
#import "MainViewController.h"

#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>

#import "AppDelegate.h"

@protocol PluginAdapterDelegate <NSObject>
- (UIView*) getView;
- (UIViewController*) getViewController;
@end

@interface FacebookAds : CDVPlugin <PluginAdapterDelegate, FBAdViewDelegate, FBInterstitialAdDelegate, FBRewardedVideoAdDelegate>

@property(nonatomic, retain) id<PluginAdapterDelegate> adapter;
@property (nonatomic, strong) FBInterstitialAd *interstitialAd;
@property (nonatomic, strong) FBRewardedVideoAd *rewardedVideoAd;
@property (nonatomic, strong) FBAdView *adView;
@property (nonatomic, strong) UIView *bannerContainer;
@property (nonatomic, strong) CDVInvokedUrlCommand *CDVcommand;
@end

@implementation FacebookAds

CGFloat BANNER_HEIGHT = 50;
CGFloat bottomPadding = 0;

NSString *BANNER_ID;
NSString *INTERSTITIAL_ID;
NSString *REWARDED_ID;

NSString *BANNER = @"BANNER";
NSString *INTERSTITIAL = @"INTERSTITIAL";
NSString *REWARDED = @"REWARDED";
NSString *TEST_MODE = @"TEST_MODE";
NSString *LOGS = @"LOGS";

NSString *STATUS_LOADED = @"LOADED";
NSString *STATUS_CLOSED = @"CLOSED";
NSString *STATUS_IMPRESSION = @"IMPRESSION";
NSString *STATUS_COMPLETED = @"COMPLETED";
NSString *STATUS_REWARD = @"REWARD";
NSString *STATUS_ERROR = @"ERROR";
NSString *STATUS_ON = @"ON";
NSString *STATUS_OFF = @"OFF";

NSString *bannerCallbackId;
NSString *interstitialCallbackId;
NSString *rewardedCallbackId;
NSString *initCallbackId;

#pragma mark - Public methods

- (void)pluginInitialize:(CDVInvokedUrlCommand *)command{
    printf("\n Plugin Initialize \n");
}

- (void)initialize:(CDVInvokedUrlCommand*)command
{
    
    //[FBAdSettings clearTestDevices];
    //[FBAdSettings addTestDevice:@"0315f79a1c58488d863ec38e9316295d96ded6b4"];
    //[FBAdSettings setLogLevel:FBAdLogLevelLog];
    initCallbackId = command.callbackId;
    [self defineBottomPadding ];
    [self createBannerContainer ];
    NSString *idfaString = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    printf("\n\n\n------ Initializing Facebook Ads Plugun ------\n");
    printf("\n------      © 2019 Statut Systems       ------\n");
    
    printf("\n\nDevice IDFA:\n");
    printf("%s", [idfaString UTF8String]);
    
    printf("\n\nDevice HASH:\n");
    printf("%s", [[FBAdSettings testDeviceHash] UTF8String]);
    printf("\n\n");
    
    int testModeRequested = [[command.arguments objectAtIndex:0] intValue];
    int logsRequested = [[command.arguments objectAtIndex:1] intValue];
    NSString* bannerId = [command.arguments objectAtIndex:2];
    NSString* interstitialId = [command.arguments objectAtIndex:3];
    NSString* rewardedId = [command.arguments objectAtIndex:4];
    
    [self handleleTestMode:testModeRequested];
    [self handleleLogs:logsRequested];
    
    BANNER_ID = bannerId;
    INTERSTITIAL_ID = interstitialId;
    REWARDED_ID = rewardedId;
    
    //    NSLog(@"%@", testMode);
    //    NSLog(@"%@", bannerId);
    //    NSLog(@"%@", interstitialId);
    //    NSLog(@"%@", rewardedId);
    //    _BANNER_ID = @"227536528163867_457924038458447";
    //    _INTERSTITIAL_ID = @"227536528163867_458093518441499";
    //    _REWARDED_ID = @"227536528163867_457925478458303";
    //    printf("\n\nARRAY:");
    //    NSLog(@"%@",command.arguments);
    
    
}

- (void)showBanner:(CDVInvokedUrlCommand*)command
{
    bannerCallbackId = command.callbackId;
    [self loadAndShowBanner];
}

- (void)hideBanner:(CDVInvokedUrlCommand*)command
{
    bannerCallbackId = command.callbackId;
    [self removeBanner];
}

- (void)showInterstitial:(CDVInvokedUrlCommand*)command
{
    interstitialCallbackId = command.callbackId;
    [self loadAndShowInterstitial];
}

- (void)showRewarded:(CDVInvokedUrlCommand*)command
{
    rewardedCallbackId = command.callbackId;
    [self loadAndShowRewarded ];
}

#pragma mark - Misc

- (void) defineBottomPadding
{
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
    }
}

- (void) sendCallback: (NSString*) callbackId : (NSString*) status
{
    CDVPluginResult* pluginResult =
    [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: status];
    [pluginResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
}

- (void) handleleTestMode: (int) testModeRequested
{
    if (testModeRequested == 1){
        [self sendCallback : initCallbackId : [NSString stringWithFormat:@"%@%@%@", TEST_MODE, @"_", STATUS_ON]];
        [self enableTestMode];
    } else {
        [self sendCallback : initCallbackId : [NSString stringWithFormat:@"%@%@%@", TEST_MODE, @"_", STATUS_OFF]];
        [self disableTestMode];
    }
    
    printf("\n\nTest mode:\n");
    printf("%d\n", [FBAdSettings isTestMode]);
}

- (void) handleleLogs: (int) logsRequested
{
    if (logsRequested == 1){
        [self sendCallback : initCallbackId : [NSString stringWithFormat:@"%@%@%@", LOGS, @"_", STATUS_ON]];
        [FBAdSettings setLogLevel:FBAdLogLevelLog];
    }
}

- (void) enableTestMode
{
    [FBAdSettings addTestDevice: [FBAdSettings testDeviceHash]];
}

- (void) disableTestMode
{
    [FBAdSettings clearTestDevice: [FBAdSettings testDeviceHash]];
}


- (void) resizeWebView
{
    // CGFloat test = 2;
    CGFloat screenWidth = [self getView].superview.frame.size.width;
    CGFloat screenHeight = [self getView].superview.frame.size.height;
    CGFloat resultHeight = screenHeight;
    
    if(!self.bannerContainer.hidden) {
        resultHeight = screenHeight - BANNER_HEIGHT - bottomPadding;
        
        
    }
    
    
    
    [self.webView setFrame:CGRectMake(0, 0, screenWidth, resultHeight)];
    
}

- (UIView*) getView
{
    if(self.adapter) return [self.adapter getView];
    else return self.webView;
}



- (UIViewController*) getViewController
{
    if(self.adapter) return [self.adapter getViewController];
    else return self.viewController;
}




#pragma mark - Banner

- (void)createBannerContainer {
    
    self.bannerContainer=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    [self.bannerContainer setBackgroundColor:[UIColor whiteColor]];
    
    [[self getView].superview addSubview:self.bannerContainer];
    
    self.bannerContainer.layer.anchorPoint = CGPointMake(.5, 1);
    self.bannerContainer.center = CGPointMake([self getView].superview.frame.size.width  / 2,
                                              [self getView].superview.frame.size.height - bottomPadding );
    self.bannerContainer.hidden = YES;
};

- (void)removeBanner
{
    self.bannerContainer.hidden = YES;
    [self resizeWebView];
    [self.adView removeFromSuperview];
}

- (void) loadAndShowBanner
{
    self.adView = [[FBAdView alloc]
                   initWithPlacementID:BANNER_ID
                   adSize:kFBAdSizeHeight50Banner
                   rootViewController:[self getViewController]];
    self.adView.frame = CGRectMake(0, 0, 320, 50);
    self.adView.delegate = self;
    [self.adView loadAd];
}

- (void)adViewDidLoad:(FBAdView *)adView
{
    if (self.adView && self.adView.isAdValid) {
        self.bannerContainer.hidden = NO;
        [self resizeWebView];
        [self sendCallback : bannerCallbackId : STATUS_LOADED];
        [self.bannerContainer addSubview: self.adView];
    }
}

- (void)adViewWillLogImpression:(FBAdView *)adView
{
    [self sendCallback : bannerCallbackId : STATUS_IMPRESSION];
}

- (void)adView:(FBAdView *)adView didFailWithError:(NSError *)error
{
    [self sendCallback : bannerCallbackId : STATUS_ERROR];
    NSLog(@"%@",[error localizedDescription]);
}

#pragma mark - Interstitial

- (void) loadAndShowInterstitial
{
    self.interstitialAd = [[FBInterstitialAd alloc] initWithPlacementID:INTERSTITIAL_ID];
    self.interstitialAd.delegate = self;
    [self.interstitialAd loadAd];
}

- (void)interstitialAdWillLogImpression:(FBInterstitialAd *)interstitialAd
{
    [self sendCallback : interstitialCallbackId : STATUS_IMPRESSION];
}


- (void)interstitialAdDidClose:(FBInterstitialAd *)interstitialAd
{
    [self sendCallback : interstitialCallbackId : STATUS_CLOSED];
}

- (void)interstitialAdDidLoad:(FBInterstitialAd *)interstitialAd
{
    if (interstitialAd && interstitialAd.isAdValid) {
        [self sendCallback : interstitialCallbackId : STATUS_LOADED];
        [interstitialAd showAdFromRootViewController:[self getViewController]];
    }
}

- (void)interstitialAd:(FBInterstitialAd *)interstitialAd didFailWithError:(NSError *)error
{
    [self sendCallback : interstitialCallbackId : STATUS_ERROR];
    NSLog(@"%@",[error localizedDescription]);
}


#pragma mark - Rewarded

- (void) loadAndShowRewarded
{
    self.rewardedVideoAd = [[FBRewardedVideoAd alloc] initWithPlacementID:REWARDED_ID];
    self.rewardedVideoAd.delegate = self;
    [self.rewardedVideoAd loadAd];
}

- (void)rewardedVideoAdDidLoad:(FBRewardedVideoAd *)rewardedVideoAd
{
    if (rewardedVideoAd && rewardedVideoAd.isAdValid) {
        [self sendCallback : rewardedCallbackId : STATUS_LOADED];
        [rewardedVideoAd showAdFromRootViewController:[self getViewController]];
    }
}

- (void)rewardedVideoAdVideoComplete:(FBRewardedVideoAd *)rewardedVideoAd;
{
    [self sendCallback : rewardedCallbackId : STATUS_REWARD];
}

- (void)rewardedVideoAdDidClose:(FBRewardedVideoAd *)rewardedVideoAd
{
    [self sendCallback : rewardedCallbackId : STATUS_CLOSED];
}

- (void)rewardedVideoAdWillLogImpression:(FBRewardedVideoAd *)rewardedVideoAd
{
    [self sendCallback : rewardedCallbackId : STATUS_IMPRESSION];
}

- (void)rewardedVideoAd:(FBRewardedVideoAd *)rewardedVideoAd didFailWithError:(NSError *)error
{
    [self sendCallback : rewardedCallbackId : STATUS_ERROR];
    NSLog(@"%@",[error localizedDescription]);
}

@end
