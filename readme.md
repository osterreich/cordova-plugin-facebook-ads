Facebook Audience Network Cordova Plugin

![Image](fan-logo.png "Facebook Audience Network Logo")

Author: Nikita Filipenia

## Platforms

* iOS

## Initializing plugin

        window.FacebookAds.initialize(
            {
                BANNER_ID: 'PLACEMENT_ID',
                INTERSTITIAL_ID: 'PLACEMENT_ID',
                REWARDED_ID: 'PLACEMENT_ID',
                testMode: true,
                logs: true
            },
            (result) => {
                console.log("Result: ", result);
            });

## Troubleshooting

*In progress* 

## Reference

**Initialize plugin**

    window.FacebookAds.initialize(options, callback);

*Available options*

    BANNER_ID: String
    INTERSTITIAL_ID: String
    REWARDED_ID: String
    testMode: Bool (optional)
    logs: Bool (optional)

*Callback events*

    TEST_MODE_ON
    TEST_MODE_OFF
    LOGS_ON

**Show Banner (320x50)**

    window.FacebookAds.showBanner(callback);

*Callback events*

    LOADED
    IMPRESSION
    ERROR

**Hide Banner**

    window.FacebookAds.hideBanner();

**Show Interstitial ad**

    window.FacebookAds.showInterstitial(callback);

*Callback events*

    LOADED
    IMPRESSION
    CLOSED
    ERROR

**Show Rewarded video ad**

    window.FacebookAds.showRewarded(callback);

*Callback events*

    LOADED
    IMPRESSION
    REWARD
    CLOSED
    ERROR

© 2019 Statut Systems