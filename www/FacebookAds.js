var exec = require('cordova/exec');

exports.initialize = function (parameters, callback) {
    var args = [
        !!parameters.testMode,
        !!parameters.logs,
        parameters.BANNER_ID || '',
        parameters.INTERSTITIAL_ID || '',
        parameters.REWARDED_ID || '',
    ]
    exec(callback, null, 'FacebookAds', 'initialize', args);
};

exports.showBanner = function (callback) {
    exec(callback, null, 'FacebookAds', 'showBanner', []);
};

exports.hideBanner = function (callback) {
    exec(callback, null, 'FacebookAds', 'hideBanner', []);
};

exports.showInterstitial = function (callback) {
    exec(callback, null, 'FacebookAds', 'showInterstitial', []);
};

exports.showRewarded = function (callback) {
    exec(callback, null, 'FacebookAds', 'showRewarded', []);
};